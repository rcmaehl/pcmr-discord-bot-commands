import json
import sys
import urllib.request
import os
from pprint import pprint

# These hosts provide permanent links to images that cannot change.  We don't want
# a command's image to be changed without a merge so someone can't put a bad image in later
valid_image_hosts = ["https://gitlab.com/",
                     "https://cdn.discordapp.com",
                     "/"]

headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)'}

errors = []
commands = None

try:
    commands = json.load(open('commands.json'))
except Exception as e:
    errors.append(e)

if commands:
    for command in commands:
        # Ensure each command sends some text.
        if 'text' not in commands[command] and "noembed" not in commands[command].get('attribs', []):
            errors.append(f"{command} is missing the 'text' value.  This must be set.  Alternatively set the \"noembed\" attribute.")
    
        # Validate that all images are in valid_image_hosts.
        if 'image' in commands[command] and commands[command]['image']:
            for host in valid_image_hosts:
                if commands[command]['image'].startswith(host):
                    break
            else:
                errors.append(f"{command}'s image attribute is not hosted by a valid image host.")
            
            if commands[command]['image'][0] == "/": # Locally hosted on Gitlab.
                imageurl = f"https://gitlab.com/{os.environ.get('CI_PROJECT_PATH')}/-/raw/{os.environ.get('CI_COMMIT_BRANCH')}{commands[command]['image']}"
            else:
                imageurl = commands[command]['image']

            try:
                req = urllib.request.Request(imageurl, None, headers)
                with urllib.request.urlopen(req) as response:
                    html = response.read()
            except Exception as e:
                # Ok, this might be a PR that is adding this image.  Try a URL.
                errors.append(f"{command}'s image attribute could not be loaded by CI/CD. {e}\n{imageurl}")

if errors:
    pprint(os.environ)
    print("\nERROR: ".join(errors))
    sys.exit(1)
